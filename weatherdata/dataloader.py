import pandas
from pandas import DataFrame

import math

class Dataloader():
    
    """ Loads data provided by  """
    def __init__(
            self,
            rawdata: str,
            headerfile: str
        ):

        #.. Initialization 
        self._datapath = rawdata
        self._headerpath = headerfile
        self.data = DataFrame()

        #.. Load data
        self.data = pandas.read_csv(
            self._datapath,
            sep='\\s+',
            header=None,
            #parse_dates=[[0, 1, 2, 3]]
        )
        
        #--- FORMAT DATE ---#
        #.. Combine the desired date columns into a single datetime column
        self.data['foo'] = pandas.to_datetime(
                self.data[[0, 1, 2, 3]].astype(str).agg(' '.join, axis=1), 
                format="%Y %m %d %H"
        )
        
        #.. remove the first 4 columns
        self.data.drop(columns=[0, 1, 2, 3], inplace=True)
        
        #.. rearrange the columns that Datetime is the first column
        self.data = self.data[['foo'] + list(self.data.columns.difference(['foo']))]
        #-------------------#

        #--- DATA HEADER AND INDEX ---#
        #.. Loads header 
        header = self._load_header(self._headerpath)
        
        #.. Creates callable labels
        self.LABEL_DATE     = list(self._headerlabel("Date", header).keys())[0]
        self.LABEL_TEMP     = list(self._headerlabel("Temp", header).keys())[0]
        self.LABEL_DEW      = list(self._headerlabel("Dew",  header).keys())[0]
        self.LABEL_PRESSURE = list(self._headerlabel("Pres", header).keys())[0] 
        self.LABEL_WIND     = list(self._headerlabel("Speed",header).keys())[0] 
        self.LABEL_WINDDIR  = list(self._headerlabel("Direc",header).keys())[0] 
        self.LABEL_RAIN_1H  = list(self._headerlabel("1h",   header).keys())[0] 
        self.LABEL_RAIN_6H  = list(self._headerlabel("6h",   header).keys())[0] 
 
        ##.. Set column header accordingly to the loaded headers
        self.data.columns = list(header.keys())

        #.. Set the index
        self.data.set_index(self.LABEL_DATE,inplace=True)
        #-----------------------------#
        
        #--- DATA SETUP ---#
        #.. Correct the data and make it SI conform + °C
        self.data = self._setColumneType(self.data, self.LABEL_WINDDIR, float)
        self.data = self._removeNoneExsitingValues(self.data)
        self.data = self._correctWindDirections(self.data, self.LABEL_WINDDIR)      
        self.data = self._removeNegativeRain(self.data, self.LABEL_RAIN_1H, self.LABEL_RAIN_6H)
        
        #.. Corrects values
        self.data = self._correctValuesByFactor(self.data, self.LABEL_TEMP,0.1) 
        self.data = self._correctValuesByFactor(self.data, self.LABEL_DEW,0.1) 
        self.data = self._correctValuesByFactor(self.data, self.LABEL_PRESSURE,10) 
        self.data = self._correctValuesByFactor(self.data, self.LABEL_WIND,0.1) 
        self.data = self._correctValuesByFactor(self.data, self.LABEL_RAIN_1H,0.1) 
        self.data = self._correctValuesByFactor(self.data, self.LABEL_RAIN_6H,0.1) 
        
        #.. Adds missing times
        self.data = self._addMissingTimes(self.data)
        #------------------#

    #=============#
    #== Methods ==#
    #=============#
    def _headerlabel(self, lookup: str, data: dict):
        output = {}
        for key, value in data.items():
            if lookup in key:
                output[key] = value
    
        return output
 
    #-------------#
    def _load_header(self, headerpath):
        
        header_list = []
        with open(headerpath,"r") as file:
            for line in file.readlines():
                line_list = [str(x).rstrip().lstrip() for x in line.split(',')]
                header_list.append(line_list)

        return {header_list[0][i]: header_list[1][i] for i in range(len(header_list[0]))}

    #-------------#
    def _setColumneType(self, df:DataFrame, LABEL, newtype):
        df[LABEL] = df[LABEL].astype(newtype)
        return df
    
    #-------------#
    def _removeNoneExsitingValues(self, df: DataFrame):
        df.replace(
            {
                -9999:math.nan
            },
            inplace=True
        )
        return df

    #-------------#
    def _correctWindDirections(self, df: DataFrame, label: str):
        df[label] = df[label].replace(
            {
                0:math.nan,
                360:0
            }
        )
        return df

    #-------------#
    def _removeNegativeRain(self, df: DataFrame, LABEL_RAIN_1H, LABEL_RAIN_6H):
        
        for val in df[df[LABEL_RAIN_1H] < 0.0].index:
            df.loc[val,LABEL_RAIN_1H] = 0.0

        for val in df[df[LABEL_RAIN_6H] < 0.0].index:
            df.loc[val,LABEL_RAIN_6H] = 0.0

        return df
    
    #-------------#
    def _correctValuesByFactor(self, df: DataFrame, LABEL: str, factor):

        df[LABEL] = df[LABEL] * factor

        return df

    
    #-------------#
    def _addMissingTimes(self, df: DataFrame):
        
        year = df.first_valid_index().year

        #.. (A) generating a list of all timestamps in a year
        timestamp_start = f"{year}-01-01 00:00"
        timestamp_end   = f"{year}-12-31 23:00"

        expected_timestamps = pandas.date_range(
            start=timestamp_start, 
            end=timestamp_end, freq="h"
        )

        #.. (B) identify missing data and treat missing data
        for timestamp in expected_timestamps:

            #.. (df.index == timestamp).any() , if any value is true the whole expression is True
            if not (df.index == timestamp).any():
                
                #.. generates a empty Series with the current timestamp as an index
                new_row = DataFrame(index=[timestamp], columns=df.columns)
                
                #.. add data to the DataFrame, missing data will be filled with NaN
                df = pandas.concat([df, new_row.astype(df.dtypes)])

        # Sort by data
        df.sort_index(inplace=True)
 
        return df

